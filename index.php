<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Carlos Vallejo Márquez</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/simple-slideshow-styles.css">
	
</head>
<body>
	<div class="container">

		<header>
			<div class="logo"><img src="img/logoColegioSlogan.png"></div>
			<div class="nombre">
				<h2>COLEGIO</h2>
				<h2>CARLOS VALLEJO</h2>
				<h2>MÁRQUEZ</h2>				
			</div>
			<div class="social">
				<a href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a>
				<a href="#"><i class="fab fa-twitter"></i></a>
				<a href="#" id="icon_msj"><i class="far fa-envelope"></i></a>
			</div>
		</header>
		<nav id="menu">
			<div class="icon"><i class="fas fa-bars btnMenu"></i></div>
			<ul class="lista">

				<li><a href="">INICIO</a></li>
				<li><a href="#vision">ACERCA DE</a></li>
				<li><a href="#nivel">OFERTA EDUCATIVA</a></li>
				<li><a href="">TALLERES</a></li>
				<li><a href="">SERVICIOS ESCOLARES</a></li>
				<li><a href="#" id="icon_msj2">CONTACTO</a></li>
			</ul>
		</nav>
		<div class="bss-slides num1" tabindex="1" autofocus="autofocus">
            <figure>
		      <img src="img/cole/banner/banner1.jpg" width="100%" />
            </figure>
            <figure>
		      <img src="img/cole/banner/banner2.jpg" width="100%" />
            </figure>
            <figure>
		      <img src="img/cole/banner/banner3.jpg" width="100%" />
            </figure>
            <figure>
		      <img src="img/cole/banner/banner4.jpg" width="100%" />
            </figure>
            <figure>
		      <img src="img/cole/banner/banner5.jpg" width="100%" />
            </figure>
        </div> <!-- // bss-slides -->  


        <!--EMPIEZA RECUAROS NIVEL EDUCATIVO -->
        <article class="nivel" id="nivel">
        	
        	<div class="pre">
        		<img width="150" src="img/pre.png">
        		<H2>PREESCOLAR</H2>
        		<span>Clave: CCT.17PJN0549Z.</span>
        		<p>Queremos ser el inicio de una gran aventura en la formación de tus cimientos</p>
        		<img width="40" src="https://www.tonyontstopt.be/uploads/157x0_68x0/bouncing-arrow-white.gif">
        	</div>
        	<div class="prima">
        		<img width="150" src="img/pri.png">
        		<H2>PRIMARIA</H2>
        		<span>Clave: CCT.17PPR0492E.</span>
        		<p>Nuestra mision es formar valores integrales en cada alumno...</p>
        		<img width="40" src="https://www.tonyontstopt.be/uploads/157x0_68x0/bouncing-arrow-white.gif">
        	</div>
        </article>
        <!--TERMINA RECUADROS NIVEL EDUCATIVO -->


        <!-- EMPIEZA ARTICULO PREESCOLAR-->

        <div class="p_inf" id="p_info">
        	<div class="titleClose"> 
        		<h1>PREESCOLAR</h1>
        		<div id="closePre"><i  class="fas fa-times"></i></div>
        	</div>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione rem ex debitis, deleniti, reiciendis ad, sapiente quod minima ullam ut possimus perferendis recusandae. Asperiores rem molestiae quas consequatur quae nam.
				Lorem

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			<div class="oneImg">
				<img width="450" height="350" src="img/cole/b16.jpg">
			</div>
			
			
			<div class="images bss-slides num3" tabindex="1" autofocus="autofocus">
	            <figure>
			      <img  height="400" src="img/cole/b7.jpg" width="100%" />
	            </figure>
	            <figure>
			      <img  height="400" src="img/cole/b8.jpg" width="100%" />
	            </figure>
	            <figure>
			      <img  height="400" src="img/cole/b10.jpg" width="100%" />
	            </figure>
	             <figure>
			      <img  height="400" src="img/cole/b12.jpg" width="100%" />
	            </figure>
        	</div> <!-- // bss-slides -->  
        	<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione rem ex debitis, deleniti, reiciendis ad, sapiente quod minima ullam ut possimus perferendis recusandae. Asperiores rem molestiae quas consequatur quae nam.
				Lorem

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
        	
        </div>
		<!-- TERMINA ARTICULO PREESCOLAR-->

		<!-- EMPIEZA ARTICULO PRIMARIA-->

        <div class="p_inf" id="pr_info">
        	<div class="titleClose"> 
        		<h1>PRIMARIA</h1>
        		<div id="closePrima"><i  class="fas fa-times"></i></div>
        	</div>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione rem ex debitis, deleniti, reiciendis ad, sapiente quod minima ullam ut possimus perferendis recusandae. Asperiores rem molestiae quas consequatur quae nam.
				Lorem

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			<div class="oneImg">
				<img width="450" height="350" src="img/cole/b11.jpg">
			</div>
			
			
			<div class="images bss-slides num3" tabindex="1" autofocus="autofocus">
	            <figure>
			      <img  height="400" src="img/cole/b14.jpg" width="100%" />
	            </figure>
	            <figure>
			      <img  height="400" src="img/cole/b15.jpg" width="100%" />
	            </figure>
	            <figure>
			      <img  height="400" src="img/cole/b17.jpg" width="100%" />
	            </figure>
	            
        	</div> <!-- // bss-slides -->  
        	<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione rem ex debitis, deleniti, reiciendis ad, sapiente quod minima ullam ut possimus perferendis recusandae. Asperiores rem molestiae quas consequatur quae nam.
				Lorem

				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
        	
        </div>
        <!--TERMINA ARTICULO PRIMARIA-->

        <article class="vision" id="vision">
        	
        	<div class="letras">
        		<p>NUESTRO PROPÓSITO ES LA FORMACIÓN DE LIDERES DE EXELENCIA</p>
        		<H1>CARLOS VALLEJO MÁRQUEZ</H1>
        		<p>Tiene el compromiso de brindar calidad en educación en educacion cultivando con amor el area  efectivo, social y cognocivo del alumno.</p>
        		<p>Con el firme propósito de instruir al alumno en las iferentes esferas del 
        		conocimiento para que logre un desarrollo integral y pueda cimentar bases sólidas para su vida.</p>
        	</div>

        	<div class="video">
        		<img src="img/videologo.png">
        	</div>
        </article>

       
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3175.3626690431533!2d-99.18613424066575!3d18.835379790132574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85ce7624133a1e3d%3A0x220547f1daebc1d5!2sCalle+Benito+Ju%C3%A1rez+54%2C+Centro%2C+62760+Emiliano+Zapata%2C+Mor.!5e0!3m2!1ses-419!2smx!4v1537248830461" width="100%" height="300px" frameborder="0" style="border:0" allowfullscreen></iframe>

    <footer>
    	<div class="f-left"><img src="img/logo.png" width="70" height="70"></div>
    	<div class="f-right">
    	 	<span><i class="far fa-envelope"></i> ccvallejomarquez@gmail.com</span>
    		<span><i class="fas fa-phone"></i> 777-385-327-6</span>
    	</div>
    	
    	

    </footer>
	    <div class="mensaje" id="msj_correo" >
		    <div class="msj_close"> 
        		<h1>Escribe un mensaje</h1>
        		<div id="closeMsj"><i  class="fas fa-times"></i></div>
        	</div>
			<form class="form_msj" method="post" action="enviarCorreo.php" enctype="multipart/form-data">
				<p>
					<label for="nombre">Nombre</label>
					<input id="nombre" type="text" name="Nombre" required="required">
				</p>
				<p>
					<label for="email">Email</label>
					<input id="email" type="text" name="Email"  required="required">
				</p>
				<p><label for="mensaje">Mensaje</label></p>
				<p>
					<textarea id="mensaje" name="Mensaje" required="required"></textarea>
				</p>
				<p><input type="submit" name="" value="Enviar"></p>
			</form>
		</div>
	</div>
	<script src="js/hammer.min.js"></script><!-- for swipe support on touch interfaces -->
	<script src="js/better-simple-slideshow.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		var opts = {
		    auto : {
		        speed : 3500, 
		        pauseOnHover : true
		    },
		    fullScreen : false, 
		    swipe : true
		};

		makeBSS('.num1', opts);

		var opts2 = {
		    auto : false,
		    fullScreen : true,
		    swipe : true
		};
		makeBSS('.num2', opts2);

		$(document).ready(function(e){
					$('.btnMenu').click(function(){
						$(this).toggleClass('.fas fa-times');
						$('#menu ul').toggleClass('lista');
					});

					/*
					$('.pre').click(function(){
						$('.container #p_info').toggleClass('pre_info');
						$('.nivel').css({display:'none'});
						
					});

					
					*/

					// ************PREESCOLAR**************
					$('.pre').click(function(){
						//desaparece nivel
						$('.nivel').css({							
							'display':'none'
						});

						//aparece seccion preescolar

						$('.container #p_info').addClass('pre_info');

					});

					$('#closePre').click(function(){
						//aparece nivel
						$('.nivel').css({							
							'height':'500px',
						    'display':'flex',
						    'margin':'10px 0px',
						    'padding':'10px 0px',
						    'justify-content':'center',
						    'align-items':'center',
						    'background-color':'white',
						    'position':'relative',
						    'z-index':'100'
						});
						//desaparece seccion preescolar
						$('.container #p_info').removeClass('pre_info');

					});

					//**********terminar Preescolar***********


					//**********Primaria*****************
					$('.prima').click(function(){
						//desaparece nivel
						$('.nivel').css({							
							'display':'none'
						});

						//aparece seccion preescolar

						$('.container #pr_info').addClass('pre_info');

					});

					$('#closePrima').click(function(){
						//aparece nivel
						$('.nivel').css({							
							'height':'500px',
						    'display':'flex',
						    'margin':'10px 0px',
						    'padding':'10px 0px',
						    'justify-content':'center',
						    'align-items':'center',
						    'background-color':'white',
						    'position':'relative',
						    'z-index':'100'
						});
						//desaparece seccion preescolar
						$('.container #pr_info').removeClass('pre_info');

					});



					//termina Primaria****************


					//Ocultar Formulario Mensaje

					$('#closeMsj').click(function(){
						$('#msj_correo').removeClass('mensaje_show');
						

					});

					//Mostrar Formulario Mensaje
					$('#icon_msj').click(function(){
						$('#msj_correo').addClass('mensaje_show');
						$(body).css({
							'background-color': 'rgba(0,0,0,0.8)'
						});

					});

					//Mostrar Formulario Mensaje
					$('#icon_msj2').click(function(){
						$('#msj_correo').addClass('mensaje_show');

					});




				});

		//Banner 2
		var optsDos = {
		    auto : {
		        speed : 3500, 
		        pauseOnHover : true
		    },
		    fullScreen : false, 
		    swipe : true
		};
		
		makeBSS('.num3', optsDos);

		var opts3 = {
		    auto : false,
		    fullScreen : true,
		    swipe : true
		};
		makeBSS('.num2', opts3);
		

</script>


</body>
</html>